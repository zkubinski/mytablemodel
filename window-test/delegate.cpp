#include "delegate.h"

Delegate::Delegate(QObject *parent) : QItemDelegate(parent)
{
}

QWidget *Delegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option)
    Q_UNUSED(index)

    QLineEdit *lineEdit = new QLineEdit(parent);
    // connect(lineEdit, &QLineEdit::textChanged, this, &Delegate::commitAndCloseEditor);
    QWidget *lineEditToWidget = static_cast<QWidget*>(lineEdit);

    return lineEditToWidget;
}

void Delegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QLineEdit *lineEdit = static_cast<QLineEdit*>(editor);
    QString value = index.model()->data(index, Qt::EditRole).toString();
    lineEdit->setText(value);
}

void Delegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QLineEdit *lineEdit = static_cast<QLineEdit*>(editor);
    model->setData(index, lineEdit->text());
    emit lineEdit->editingFinished();
}

void Delegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index)

    editor->setGeometry(option.rect);
}

// void Delegate::commitAndCloseEditor()
// {
//     QLineEdit *lineEdit = qobject_cast<QLineEdit*>(sender());
//     emit commitData(lineEdit);
//     emit closeEditor(lineEdit);
// }
