#ifndef DELEGATE_H
#define DELEGATE_H

#include <QItemDelegate>
#include <QObject>
#include <QLineEdit>

class Delegate : public QItemDelegate
{
    Q_OBJECT
public:
    Delegate(QObject *parent=nullptr);
    // QAbstractItemDelegate interface
protected:
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
    // void commitAndCloseEditor();
};

#endif // DELEGATE_H
