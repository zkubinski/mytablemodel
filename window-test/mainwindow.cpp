#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    this->resize(800,600);
    mainWidget = new QWidget(this);
    vLayout = new QVBoxLayout(mainWidget);

    myModel = new ItemModel(this);
    myView = new MyTableView(myModel,this);
    // view = new QTableView(this);
    myDelegate = new Delegate(this);

    bttLayout = new QHBoxLayout();
    // bttAddRow = new QPushButton(QString(tr("Add row")),this);
    // bttRemoveRow = new QPushButton(QString(tr("Remove row")),this);

    // bttAddColumn = new QPushButton(QString(tr("Add column")),this);
    // bttRemoveColumn = new QPushButton(QString(tr("Remove column")),this);

    bttClose = new QPushButton(QString(tr("Close")),this);
    bttClose->setIcon(QIcon(QPixmap(":icons/exit.png")));

    // myView->setModel(myModel);
    myView->setItemDelegate(myDelegate);

    // view->setModel(myModel);
    // view->setItemDelegate(myDelegate);
    // selectionModel = view->selectionModel();

    // vLayout->addWidget(view);
    vLayout->addWidget(myView);
    vLayout->addLayout(bttLayout);

    // bttLayout->addWidget(bttAddRow);
    // bttLayout->addWidget(bttRemoveRow);
    // bttLayout->addWidget(bttAddColumn);
    // bttLayout->addWidget(bttRemoveColumn);
    bttLayout->addWidget(bttClose);

    this->setCentralWidget(mainWidget);

    // QObject::connect(bttAddRow, &QPushButton::clicked, this, [&](){myModel->insertRow(1,QModelIndex());});
    // QObject::connect(bttRemoveRow, &QPushButton::clicked, this, [&](){myModel->removeRow(right.row(),QModelIndex());});

    QObject::connect(bttClose, &QPushButton::clicked, this, &QMainWindow::close);
}

MainWindow::~MainWindow()
{
    delete mainWidget;
}
